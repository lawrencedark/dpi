/* eslint camelcase: 0,no-unused-vars: 0 */
// import * as $ from 'cheerio'

import * as H from 'hyperscript'

import AssetLibrary from '../repeatcreator/node_modules/openfl/lib/openfl/utils/AssetLibrary'
import AssetManifest from '../repeatcreator/node_modules/openfl/lib/openfl/utils/AssetManifest'
import Assets from '../repeatcreator/node_modules/openfl/lib/openfl/utils/Assets'
import Bitmap from '../repeatcreator/node_modules/openfl/lib/openfl/display/Bitmap'
import BitmapData from '../repeatcreator/node_modules/openfl/lib/openfl/display/BitmapData'
import ColorTransform from '../repeatcreator/node_modules/openfl/lib/openfl/geom/ColorTransform'
import DOMSprite from '../repeatcreator/node_modules/openfl/lib/openfl/display/DOMSprite'
import Matrix from '../repeatcreator/node_modules/openfl/lib/openfl/geom/Matrix'
import Point from '../repeatcreator/node_modules/openfl/lib/openfl/geom/Point'
import QuickSettings from '../repeatcreator/dist/js/quicksettings.min.js'
import Rectangle from '../repeatcreator/node_modules/openfl/lib/openfl/geom/Rectangle'
import Sprite from '../repeatcreator/node_modules/openfl/lib/openfl/display/Sprite'
import Stage from '../repeatcreator/node_modules/openfl/lib/openfl/display/Stage'
import int from 'int'

// Data binding declarations to simulate MESH.mxml

let designViewFactor
let lastDesignViewWidth
let lastDesignViewHeight
let maxDesignView = 8000
let tempPreviewBMD = new BitmapData()
let xRepStart
let xRepRemainder
let yRepStart
let yRepRemainder
let xDrop
let yDrop
let bkgPixelColor
let hiloLightValue = -30
let previewImg = new BitmapData()
let previewImgW = 8000
let previewImgH = 8000
let compImgW = 8000
let compImgH = 8000
var designBitMapData = new BitmapData()
let designH = 8000
let designW = 8000
let thumbBMD = new BitmapData()
let thumbWidth = 80
let thumbHeight = 80
let repW
let repH
let dropValue
let TCInc = document.getElementById('TCInc')
let TLInc = document.getElementById('TLInc')
let TLBO = document.getElementById('TLBO')
let TCBO = document.getElementById('TCBO')
let TRBO = document.getElementById('TRBO')
let TRInc = document.getElementById('TRInc')
let MLInc = document.getElementById('MLInc')
let MLBO = document.getElementById('MLBO')
let MRInc = document.getElementById('MRInc')
let MRBO = document.getElementById('MRBO')
let BLInc = document.getElementById('BLInc')
let BLBO = document.getElementById('BLBO')
let BCInc = document.getElementById('BCInc')
let BCBO = document.getElementById('BCBO')
let BRInc = document.getElementById('BRInc')
let BRBO = document.getElementById('BRBO')
let drop = document.getElementById('drop')
let dropXY = document.getElementById('dropXY')
let repWSl = document.getElementById('repWSl')
let repHSl = document.getElementById('repHSl')
let repZSl = document.getElementById('repZSl')
let sectBMD = new BitmapData()
let repMsg = document.getElementById('repMsg')
let printDrop = document.getElementsByClassName('printDrop')
let advanced = document.getElementById('advanced')
let designWindow = document.getElementById('designWindow')
let repeatContainer = document.getElementById('repeatContainer')
let designView = document.getElementById('designView')
let repDesignXY = document.getElementById('repDesignXY')
let resXY = document.getElementById('resXY')
let thumbNail = document.getElementById('thumbNail')
let vSlider = document.getElementById('vSlider')
let hSlider = document.getElementById('hSlider')
let printUnit = document.getElementById('printUnit')
let designXY = document.getElementById('designXY')
var designZoom = document.getElementById('designZoom')
let printRepL
let printRepW
let designViewTools = document.getElementById('designViewTools')
let printContainer
let printContainerWindowChanged
let updateStyle

function setupUI () {
  vSlider.style.visibility = 'visible'
  hSlider.style.visibility = 'visible'
  designWindow.style.visibility = 'visible'
  repeatContainer.style.visibility = 'visible'
  designViewTools.style.visibility = 'visible'
}

function initRepeatImageVars () {
  repWSl.maximum = designW - 30
  repWSl.minimum = int(designW / 2) + 1
  // initial repeat set_to maximum size
  repWSl.value = repWSl.maximum
  repW = repWSl.maximum
  repHSl.maximum = designH - 30
  repHSl.minimum = int(designH / 2) + 1
  // initial repeat set_to maximum size
  repHSl.value = repHSl.maximum
  repH = repHSl.maximum
  dropChanged()
  updateRepMsg(' - ', 'Info')
  displayThumb()
}

function updateRepMsg (msg, type) {
  let repMsg = document.getElementById('repMsg')
  switch (type) {
    case 'Info':
      repMsg.style.color = '#FF0000'
      if (msg === ' - ') {
        repMsg.style.visibility = false
      } else {
        repMsg.style.visibility = true
        repMsg.innerText = msg
      }
      break
    case 'Error':
      repMsg.style.visibility = true
      repMsg.style.color = '#FF0000'
      repMsg.innerText = msg
      break
  }
}

/**
 * * This is a crucial part of the update loop!
 */
function sliderChange () {
  repW = int(repWSl.value)
  repH = int(repHSl.value)
  setDrop()
  displayThumb()
}

function plusMinus (e) {
  switch (e.currentTarget.id) {
    case 'ZO':
      repZSl.value = repZSl.value - 1
      break
    case 'ZI':
      repZSl.value = repZSl.value + 1
      break
    case 'PW':
      repWSl.value = repWSl.value + 1
      break
    case 'MW':
      repWSl.value = repWSl.value - 1
      break
    case 'PH':
      repHSl.value = repHSl.value + 1
      break
    case 'MH':
      repHSl.value = repHSl.value - 1
      break
  }
  sliderChange()
}

/**
 * * Other part of the main loop!
 */
function dropChanged () {
  setDrop()
  displayThumb()
}

function setDrop () {
  let drop = document.getElementById('drop')
  let dropXY = document.getElementById('dropXY')
  let printDrop = document.getElementsByClassName('printDrop')
  switch (drop.selectedIndex.toString()) {
    case '0':
      dropValue = 0
      dropXY.enabled = false
      updateRepMsg(' - ', 'Info')
      xDrop = 0
      yDrop = 0
      printDrop.selectedIndex = 0
      break
    case '1':
      dropXY.enabled = true
      switch (dropXY.selectedIndex.toString()) {
        case '0':
          dropValue = repHSl.value / 2
          if ((repH % 2) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 1
          break
        case '1':
          dropValue = repHSl.value / 3
          if ((repHSl.value % 3) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 2
          break
        case '2':
          dropValue = repHSl.value / 4
          if ((repHSl.value % 4) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 3
          break
      }
      yDrop = dropValue
      xDrop = 0
      break
    case '2':
      dropXY.enabled = true
      switch (dropXY.selectedIndex.toString()) {
        case '0':
          dropValue = repWSl.value / 2
          if ((repWSl.value % 2) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 4
          break
        case '1':
          dropValue = repWSl.value / 3
          if ((repWSl.value % 3) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 5
          break
        case '2':
          dropValue = repWSl.value / 4
          if ((repWSl.value % 4) > 0) {
            updateRepMsg("Drop doesn't divide equally !", 'Error')
          } else {
            updateRepMsg(' - ', 'Info')
          }
          printDrop.selectedIndex = 6
          break
      }
      xDrop = dropValue
      yDrop = 0
      break
  }
}

function calculateRepeat () {
  let xOffset
  let yOffset
  let destPixClr
  let srcPixClr
  tempPreviewBMD = new BitmapData(repW, repH, true, 0xFFFFFFFF)
  tempPreviewBMD.copyPixels(designBitMapData, new Rectangle(xRepStart, yRepStart, repW, repH), new Point(0, 0))
  tempPreviewBMD.lock()
  let advanced = document.getElementById('advanced')
  for (let index = 1; index <= 8; index++) {
    const fobj = advanced.childNodes[index]
    const sectID = fobj.id
    switch (sectID) {
      case 'TL': // top left
        if (TLInc.selected === true) {
          sectBMD = new BitmapData(xRepStart, yRepStart, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(0, 0, xRepStart, yRepStart), new Point(0, 0))
          xOffset = (repW - xRepStart) + xDrop
          yOffset = (repH - yRepStart) + yDrop
          for (let xTL = 0; xTL < xRepStart; xTL++) {
            for (let yTL = 0; yTL < yRepStart; yTL++) {
              srcPixClr = sectBMD.getPixel(xTL, yTL)
              if (srcPixClr !== bkgPixelColor) {
                if (TLBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xTL) % repW), ((yOffset + yTL) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xTL) % repW), ((yOffset + yTL) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xTL) % repW), ((yOffset + yTL) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'TC': // top center
        if (TCInc.selected === true) {
          sectBMD = new BitmapData(repW, yRepStart, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(xRepStart, 0, repW, yRepStart), new Point(0, 0))
          xOffset = xDrop
          yOffset = (repH - yRepStart)
          for (let xTM = 0; xTM < repW; xTM++) {
            for (let yTM = 0; yTM < yRepStart; yTM++) {
              srcPixClr = sectBMD.getPixel(xTM, yTM)
              if (srcPixClr !== bkgPixelColor) {
                if (TCBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xTM) % repW), ((yOffset + yTM) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xTM) % repW), ((yOffset + yTM) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xTM) % repW), ((yOffset + yTM) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'TR': // top right
        if (TRInc.selected === true) {
          sectBMD = new BitmapData(xRepRemainder, yRepStart, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle((designW - xRepRemainder), 0, xRepRemainder, yRepStart), new Point(0, 0))
          xOffset = xDrop
          yOffset = (repH - yRepStart) + yDrop
          for (let xTR = 0; xTR < xRepRemainder; xTR++) {
            for (let yTR = 0; yTR < yRepStart; yTR++) {
              srcPixClr = sectBMD.getPixel(xTR, yTR)
              if (srcPixClr !== bkgPixelColor) {
                if (TRBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xTR) % repW), ((yOffset + yTR) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xTR) % repW), ((yOffset + yTR) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xTR) % repW), ((yOffset + yTR) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'ML': // middle left
        if (MLInc.selected === true) {
          sectBMD = new BitmapData(xRepStart, repH, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(0, yRepStart, xRepStart, repH), new Point(0, 0))
          xOffset = repW - xRepStart
          yOffset = yDrop
          for (let xML = 0; xML < xRepStart; xML++) {
            for (let yML = 0; yML < repH; yML++) {
              srcPixClr = sectBMD.getPixel(xML, yML)
              if (srcPixClr !== bkgPixelColor) {
                if (MLBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xML) % repW), ((yOffset + yML) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xML) % repW), ((yOffset + yML) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xML) % repW), ((yOffset + yML) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'MR': // middle right
        if (MRInc.selected === true) {
          sectBMD = new BitmapData(xRepRemainder, repH, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle((designW - xRepRemainder), yRepStart, xRepRemainder, repH), new Point(0, 0))
          xOffset = 0
          yOffset = repH - yDrop
          for (let xMR = 0; xMR < xRepRemainder; xMR++) {
            for (let yMR = 0; yMR < repH; yMR++) {
              srcPixClr = sectBMD.getPixel(xMR, yMR)
              if (srcPixClr !== bkgPixelColor) {
                if (MRBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xMR) % repW), ((yOffset + yMR) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xMR) % repW), ((yOffset + yMR) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xMR) % repW), ((yOffset + yMR) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'BL': // bottom left
        if (BLInc.selected === true) {
          sectBMD = new BitmapData(xRepStart, yRepRemainder, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(0, designH - yRepRemainder, xRepStart, yRepRemainder), new Point(0, 0))
          xOffset = (repW - xRepStart) + xDrop
          yOffset = yDrop
          for (let xBL = 0; xBL < xRepStart; xBL++) {
            for (let yBL = 0; yBL < yRepRemainder; yBL++) {
              srcPixClr = sectBMD.getPixel(xBL, yBL)
              if (srcPixClr !== bkgPixelColor) {
                if (BLBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xBL) % repW), ((yOffset + yBL) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xBL) % repW), ((yOffset + yBL) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xBL) % repW), ((yOffset + yBL) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'BC': // bottom center
        if (BCInc.selected === true) {
          sectBMD = new BitmapData(repW, yRepRemainder, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(xRepStart, designH - yRepRemainder, repW, yRepRemainder), new Point(0, 0))
          xOffset = repW - xDrop
          yOffset = 0
          for (let xBC = 0; xBC < repW; xBC++) {
            for (let yBC = 0; yBC < yRepRemainder; yBC++) {
              srcPixClr = sectBMD.getPixel(xBC, yBC)
              if (srcPixClr !== bkgPixelColor) {
                if (BCBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xBC) % repW), ((yOffset + yBC) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xBC) % repW), ((yOffset + yBC) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xBC) % repW), ((yOffset + yBC) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
      case 'BR': // bottom right
        if (BRInc.selected === true) {
          sectBMD = new BitmapData(xRepRemainder, yRepRemainder, true, 0xFFFFFFFF)
          sectBMD.copyPixels(designBitMapData, new Rectangle(designW - xRepRemainder, designH - yRepRemainder, xRepRemainder, yRepRemainder), new Point(0, 0))
          xOffset = xDrop
          yOffset = yDrop
          for (let xBR = 0; xBR < xRepRemainder; xBR++) {
            for (let yBR = 0; yBR < yRepRemainder; yBR++) {
              srcPixClr = sectBMD.getPixel(xBR, yBR)
              if (srcPixClr !== bkgPixelColor) {
                if (BRBO.selected === true) {
                  destPixClr = tempPreviewBMD.getPixel(((xOffset + xBR) % repW), ((yOffset + yBR) % repH))
                  if (destPixClr === bkgPixelColor) {
                    tempPreviewBMD.setPixel(((xOffset + xBR) % repW), ((yOffset + yBR) % repH), srcPixClr)
                  }
                } else {
                  tempPreviewBMD.setPixel(((xOffset + xBR) % repW), ((yOffset + yBR) % repH), srcPixClr)
                }
              }
            }
          }
        }
        break
    }
  }
}

function stepAndRepeatDisplay () {
  let previewFactor = 1
  const matrix = new Matrix()
  previewImg.width = repeatContainer.width
  previewImg.height = repeatContainer.height
  let dropWFactor = 1
  let dropHFactor = 1
  // think about zoomFactor for showing more repeats
  switch (drop.selectedIndex.toString()) {
    case '0':
      break
    case '1':
      dropWFactor = dropXY.selectedIndex + 2
      break
    case '2':
      dropHFactor = dropXY.selectedIndex + 2
      break
  }
  const wFactor = int(repW / repeatContainer.width) + 1
  const hFactor = int(repH / repeatContainer.height) + 1
  if (hFactor >= wFactor) {
    previewFactor = hFactor
  } else {
    previewFactor = wFactor
  }
  matrix.scale(1 / previewFactor, 1 / previewFactor)
  previewImgW = int(tempPreviewBMD.width / previewFactor) * dropWFactor
  previewImgH = int(tempPreviewBMD.height / previewFactor) * dropHFactor
  compImgW = previewImgW / dropWFactor
  compImgH = previewImgH / dropHFactor
  const previewBitmapData = new BitmapData(previewImgW, previewImgH, true, 0x00FFFFFF)
  previewBitmapData.draw(tempPreviewBMD, matrix)
  let dropSectionSize
  let fromX
  let fromY
  let toX
  let toY
  switch (drop.selectedIndex.toString()) {
    case '0':
      break
    case '1':
      dropSectionSize = previewImgH - (previewImgH / (dropXY.selectedIndex + 2))
      for (let dropWRep = 0; dropWRep <= dropXY.selectedIndex; dropWRep++) {
        fromX = dropWRep * compImgW
        toX = (dropWRep + 1) * compImgW
        previewBitmapData.copyPixels(previewBitmapData,
          new Rectangle(fromX, 0, compImgW, dropSectionSize),
          new Point(toX, compImgH - dropSectionSize)
        )
        previewBitmapData.copyPixels(previewBitmapData, updateRepMsg(
          new Rectangle(fromX, dropSectionSize, compImgW, compImgH - dropSectionSize),
          new Point(toX, 0)
        ))
        break
      }
      break
    case '2':
      dropSectionSize = previewImgW - (previewImgW / (dropXY.selectedIndex + 2))
      for (let dropHRep = 0; dropHRep <= dropXY.selectedIndex; dropHRep++) {
        fromY = dropHRep * compImgH
        toY = (dropHRep + 1) * compImgH
        previewBitmapData.copyPixels(previewBitmapData,
          new Rectangle(0, fromY, dropSectionSize, compImgH),
          new Point(compImgW - dropSectionSize, toY)
        )
        previewBitmapData.copyPixels(previewBitmapData,
          new Rectangle(dropSectionSize, fromY, compImgW - dropSectionSize, compImgH),
          new Point(0, toY)
        )
      }
      break
  }
  previewImg.source = previewBitmapData
}

function displayRepeat () {
  let tempPreviewBMD = new BitmapData(repW, repH, true, 0xFFFFFFFF)
  tempPreviewBMD.copyPixels(designBitMapData, new Rectangle(xRepStart, yRepStart, repW, repH), new Point(0, 0))
  tempPreviewBMD.lock()
  calculateRepeat() // Setup repeat with processed sections
  tempPreviewBMD.unlock()
  stepAndRepeatDisplay() // Step & Repeat 'the repeat' for Display
}

/**
 * * Part of main loop!
 */
function displayThumb () {
  let thumbScale
  let thumbRepW
  let thumbRepH
  const defaultThumbWidth = 80
  const defaultThumbHeight = 80
  const aspectRatio = new Matrix()
  const thumbBMD = new BitmapData(repW, repH, true, 0xFFFFFFFF)
  if (designW > designH) {
    thumbWidth = defaultThumbWidth
    thumbScale = defaultThumbWidth / designW
    thumbHeight = int(designH * thumbScale)
  } else {
    thumbHeight = defaultThumbHeight
    thumbScale = defaultThumbHeight / designH
    thumbWidth = int(designW * thumbScale)
  }
  aspectRatio.scale(thumbScale, thumbScale)
  thumbBMD.draw(designBitMapData, aspectRatio)
  thumbRepW = repW * thumbScale
  thumbRepH = repH * thumbScale
  thumbBMD.colorTransform(new Rectangle((thumbWidth / 2) - (thumbRepW / 2), (thumbHeight / 2) - (thumbRepH / 2), thumbRepW, thumbRepH),
    new ColorTransform(1, 1, 1, 1, hiloLightValue, hiloLightValue, hiloLightValue, 0))
  const thumbNail = new Bitmap(thumbBMD)
  xRepStart = int(designW / 2) - int(repW / 2) // 0 Base
  yRepStart = int(designH / 2) - int(repH / 2) // 0 Base
  xRepRemainder = designW - (repW + xRepStart) // Absolute Size
  yRepRemainder = designH - (repH + yRepStart) // Absolute Size
  setDrop()
  displayRepeat()
}

function checkDesignPosition () {
  let designView = document.getElementById('designView')
  if (designView.y > (designWindow.height / 2)) {
    designView.y = designWindow.height / 2
  }
  if ((designView.y + designView.height) < (designWindow.height / 2)) {
    designView.y = (designWindow.height / 2) - designView.height
  }
  if (designView.x > (designWindow.width / 2)) {
    designView.x = designWindow.width / 2
  }
  if ((designView.x + designView.width) < (designWindow.width / 2)) {
    designView.x = (designWindow.width / 2) - designView.width
  }
}

function recenterDesignWindow () {
  let xOffsetMult
  let yOffsetMult
  let zoomXDif
  let zoomYDif
  var designView = BitmapData.loadFromFile('assets/floral1.png')
  designView.id = 'designView'
  if (designZoom.value === designZoom.minimum) {
    designView.x = ((designWindow.width / 2) - (designView.width / 2))
    designView.y = ((designWindow.height / 2) - (designView.height / 2))
  } else {
    if (designView.x < 0) {
      zoomXDif = (designWindow.width / 2) + (designView.x * -1)
    } else {
      zoomXDif = (designWindow.width / 2) - designView.x
    }
    if (designView.y < 0) {
      zoomYDif = (designWindow.height / 2) + (designView.y * -1)
    } else {
      zoomYDif = (designWindow.height / 2) - designView.y
    }
    xOffsetMult = lastDesignViewWidth / zoomXDif
    yOffsetMult = lastDesignViewHeight / zoomYDif
    designView.x = designView.x - ((designView.width - lastDesignViewWidth) / xOffsetMult)
    designView.y = designView.y - ((designView.height - lastDesignViewHeight) / yOffsetMult)
    checkDesignPosition()
  }
}

function resizeDesignWindow () {
  let designView = document.getElementById('designView')
  lastDesignViewWidth = designView.width
  lastDesignViewHeight = designView.height
  designViewFactor = 100 / designZoom.value
  designView.width = int(designW / designViewFactor)
  designView.height = int(designH / designViewFactor)
  recenterDesignWindow()
}

function defaultDesignWindow () {
  let designZoom = document.getElementById('designZoom')
  var designView = document.createElement('img')
  designView.id = 'designView'
  document.body.appendChild(designView)
  designZoom.maximum = 100

  if ((designH >= maxDesignView) || (designW >= maxDesignView)) {
    if (designH >= designW) {
      designZoom.maximum = int(100 / (designH / maxDesignView))
    } else {
      designZoom.maximum = int(100 / (designW / maxDesignView))
    }
  }

  if ((designH / designWindow.height) >= (designW / designWindow.width)) {
    designViewFactor = designH / designWindow.height
  } else {
    designViewFactor = designW / designWindow.width
  }

  designView.width = int(designW / designViewFactor)
  designView.height = int(designH / designViewFactor)

  designZoom.value = 100 / designViewFactor
  designZoom.minimum = 100 / designViewFactor

  resizeDesignWindow()
}

function loadDesignComplete () {
  let designXY = document.getElementById('designXY')
  let repDesignXY = document.getElementById('repDesignXY')
  let imageRes = 150
  let resXY = document.getElementById('resXY')
  let printUnit = document.getElementById('printUnit')
  var designView = document.createElement('img')
  var previewImg = document.createElement('img')
  designView.id = 'designView'
  previewImg.id = 'previewImg'
  designXY.innerText = `${designW.toString()}x X ${designH.toString()}y`
  repDesignXY.innerText = designXY.innerText
  resXY.innerText = `Resolution:${imageRes}`
  if (printUnit.selectedIndex === 0) {
    resXY.innerText = `${resXY.innerText} DPI`
  } else {
    resXY.innerText = `${resXY.innerText} DPcm`
  }
  designView.source = 'assets/floral1.png'
  // designView.style.visible = true
  // designViewTools.style.visible = true
  defaultDesignWindow()
  initRepeatImageVars()
  designWindow.addEventListener('resize', designWindowChanged)
  repeatContainer.addEventListener('resize', repeatContainerWindowChanged)
}

function designWindowChanged (Event) {
  defaultDesignWindow()
}

function repeatContainerWindowChanged (Event) {
  displayRepeat()
}

class App extends Sprite {
  constructor () {
    super()
    let designWindow = document.getElementById('designWindow')
    let designWindowSprite = new DOMSprite(designWindow)
    let repeatContainer = document.getElementById('repeatContainer')
    let repeatContainerSprite = new DOMSprite(repeatContainer)
    let designView = document.getElementById('designView')
    let designViewSprite = new DOMSprite(designView)
    let previewImg = document.getElementById('previewImg')
    let prSprite = new DOMSprite(previewImg)
    let designViewTools = document.getElementById('designViewTools')
    let dVTSprite = new DOMSprite(designViewTools)
    let dZSprite = new DOMSprite(designZoom)
    BitmapData.loadFromFile('assets/floral1.png').onComplete(bitmapData => {
      var bitmap = new Bitmap(bitmapData)
      this.addChild(bitmap)
      this.addChild(designWindowSprite)
      this.addChild(prSprite)
      this.addChild(repeatContainerSprite)
      this.addChild(designViewSprite)
      this.addChild(dVTSprite)
      this.addChild(dZSprite)
    }).onError(e => console.error(e))
  }
}

let manifest = new AssetManifest()
manifest.addBitmapData('assets/floral1.png')
manifest.addBitmapData('assets/floral2.jpg')
manifest.addBitmapData('assets/floral3.jpg')
manifest.addBitmapData('assets/floral4.jpg')

AssetLibrary.loadFromManifest(manifest).onComplete((library) => {
  Assets.registerLibrary('default', library)
  let stage = new Stage(window.innerWidth, window.innerHeight, 0xFFFFFF, App, {
    renderer: 'dom'
  })
  document.body.appendChild(stage.element)
}).onError((e) => console.log(e))
